# Bradesco Sans Webfont
----

### Install
Adicionando as fontes via bower 

```sh
$ bower install --save https://bitbucket.org/onedigital/bradescosans-webfont.git
```


### Estruturas
A estrutura do segue o seguinte padrão:

- css  
    - bradescosans.css  
- sass  
    - bradescosans.scss  
- fonts  
    - Bold   
    - Bold Italic
    - Condensed  
    - Condensed Bold 
    - Light  
    - Light Italic 
    - Medium
    - Medium Italic
    - Regular
    - Regular Italic
    - Semibold
    - Semibold Italic
    - Thin
    - Thin Italic
    - XBold
    - XBold Italic  


## USO
sempre que for usar a familia de fontes Bradesco Sans utilize o font-weight normal. veja exemplo abaixo:
```css
h2 {
  font-family: 'Bradesco Sans Bold', serif;
  font-weight: normal;
}

p {
  font-family: 'Bradesco Sans Regular', serif;
  font-weight: normal;
}
```

### SASS
Caso queira colocar o SASS no lugar do CSS sobrescreva o bower.json

```json
"overrides": {
  "bradescosans-webfont": {
    "main": [
      "scss/bradescosans.scss",
      "fonts/**/*.{eot,svg,ttf,woff,woff2}"
    ]
  }
}
```
